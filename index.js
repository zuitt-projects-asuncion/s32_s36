const express = require('express');
const mongoose = require('mongoose');
//cors - cross origin resource sharing
const cors = require('cors'); //allows backend application to be available in the frontend app
const userRoutes = require('./routes/userRoutes');
const courseRoutes = require('./routes/courseRoutes');

const port = 4000;
const app = express();

mongoose.connect("mongodb+srv://admin_asuncion:admin@cluster0.auukp.mongodb.net/bookingAPI169?retryWrites=true&w=majority", {
		useNewUrlParser: true, //task169 is the database name
		useUnifiedTopology: true

});
// Create notifications if the connection to the db is successful or not

let db = mongoose.connection;
db.on('error', console.error.bind(console, 'Connection Error'));
db.once('open', () => console.log('Connected to MongoDB'));

// middlewares
app.use(express.json());
app.use(cors());
// use our routes and group together under '/users'
app.use('/users', userRoutes);
app.use('/courses', courseRoutes);

app.listen(port, () => console.log(`Server is running at port ${port}`));
